<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProtectedController extends ApiController
{
    #[Route('/api/secret', name: 'api_secret', methods: ['POST'])]
    public function index(): Response
    {
        return $this->response(['lol' => 'kek']);
    }
}
