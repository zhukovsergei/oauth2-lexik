<?php

namespace App\Controller;

use App\Entity\Application;
use App\Repository\ApplicationRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/api/login_check", name="login-check", methods={"POST"})
     * @param UserInterface $user
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function getTokenUser(UserInterface $user, JWTTokenManagerInterface $JWTManager): JsonResponse
    {
        dd(111);
        $appRepo = $this->em->getRepository(ApplicationRepository::class);
        $myApp = $appRepo->findOneBy(['name' => 'my_app_1']);
        dd(11);
        return new JsonResponse(['token' => $JWTManager->create($myApp)]);
    }

    /**
     * @Route("/create-token-manually", name="create-token-manually", methods={"GET"})
     * @param UserInterface $user
     * @param JWTTokenManagerInterface $JWTManager
     * @return JsonResponse
     */
    public function createToken(UserInterface $user, JWTTokenManagerInterface $JWTManager, ApplicationRepository $applicationRepository): JsonResponse
    {
        $myApp = $applicationRepository->findOneBy(['name' => 'my_app_1']);
        return new JsonResponse(['token' => $JWTManager->create($myApp)]);
    }

}
